﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Vuforia;

namespace Vuforia
{

public class dataTarget : MonoBehaviour {

		public Transform TextTargetName;
		public Transform TextDescription;
		public Transform ButtonAction;
		public Transform PanelDescription;

		public AudioSource soundTarget;
		public AudioClip clipTarget;

		// Use this for initialization
		void Start()
		{
			//add Audio Source as new game object component
			soundTarget = (AudioSource)gameObject.AddComponent<AudioSource>();
			//imagem.sprite = (Sprite)Resources.Load<Sprite> ("imagens/" +imageToLoad) as Sprite;
	
		}

		// Update is called once per frame
		void Update()
		{
			StateManager sm = TrackerManager.Instance.GetStateManager();
			IEnumerable<TrackableBehaviour> tbs = sm.GetActiveTrackableBehaviours();

			foreach (TrackableBehaviour tb in tbs)
			{
				string name = tb.TrackableName;
				ImageTarget it = tb.Trackable as ImageTarget;
				Vector2 size = it.GetSize();

				Debug.Log("Active image target:" + name + "  -size: " + size.x + ", " + size.y);

				//Evertime the target found it will show “name of target” on the TextTargetName. Button, Description and Panel will visible (active)

				TextTargetName.GetComponent<Text>().text = name;
				ButtonAction.gameObject.SetActive(true);
				TextDescription.gameObject.SetActive(true);
				PanelDescription.gameObject.SetActive(true);


				//If the target name was “zombie” then add listener to ButtonAction with location of the zombie sound (locate in Resources/sounds folder) and set text on TextDescription a description of the zombie

				if(name == "metal"){
					ButtonAction.GetComponent<Button>().onClick.AddListener(delegate { playSound("sounds/Metal"); });
					TextDescription.GetComponent<Text>().text = "ETAPAS DO PROCESSO DE RECICLAGEM DO METAL 1- A empresa que realizará a reciclagem recebe o material por meio de catadores ou por outras empresas de coleta;\n" +
						"2- Na fábrica, é feita a separação do metal que chega todo misturado;\n" +
						"3- O material já separado é derretido e transformado em novos produtos;\n" +
						"Especialistas dizem que são quatro os principais metais que podem ser reciclados: alumínio, ferro, aço e cobre. No Brasil, ressalta-se que quanto ao alumínio, o que mais se aproveita são latinhas, " +
						"98% delas são recicladas. Aproximadamente 65% do ferro que a indústria utiliza vêm da reciclagem, quanto ao aço esta porcentagem cai para 30%.";
					TextTargetName.GetComponent<Text> ().color = Color.yellow;
					//PanelDescription.GetComponent<Image> ().CopyToTexture = (Sprite)Resources.Load<Sprite> ("imagens/" +imageToLoad) as Sprite;
					//PanelDescription.GetComponent<Image>().CopyToTexture = dogImg;
				}



				//If the target name was “unitychan” then add listener to ButtonAction with location of the unitychan sound (locate in Resources/sounds folder) and set text on TextDescription a description of the unitychan / robot

				if (name == "NReciclavel")
				{
					ButtonAction.GetComponent<Button>().onClick.AddListener(delegate { playSound("sounds/NReciclavel"); });
					TextDescription.GetComponent<Text>().text = "A presença do lixo não reciclável pode causar problemas como prejudicar a qualidade do produto final reciclado ou até quebrar algumas máquinas que processam o material. " +
																"Exemplo um vidro com terra não é fundida junto com o vidro, assim formam-se pedrinhas que provocam a quebra espontânea do vidro.";
					TextTargetName.GetComponent<Text> ().color = Color.grey;
				}

				if(name == "papel"){
					ButtonAction.GetComponent<Button>().onClick.AddListener(delegate { playSound("sounds/Papel"); });
					TextDescription.GetComponent<Text>().text = "As fases do processo industrial de reciclagem de papel são:\n" +
						"– Desagregação ou maceração: mistura do papel velho com água, de modo a enfraquecer as ligações entre as fibras;\n" +
						"– Depuração e lavagem: têm como objetivo eliminar os contaminantes. O processo é semelhante a peneirar o papel, com peneiras cada vez mais menores.\n" +
						"– Dispersão: são utilizadas temperaturas de 50ºC a 125ºC para dissolver os contaminantes, que são depois dispersos;\n" +
						"– Destintagem: consiste na remoção das partículas de tinta aderentes à superfície das fibras;\n" +
						"– Branqueamento: para a maioria dos produtos reciclados, a destintagem é suficiente para obter um grau de brancura adequado. Se o intuito for obter produtos de alta qualidade, pode ser feito um branqueamento a base de alvejantes.";
					TextTargetName.GetComponent<Text> ().color = Color.blue;
				}

				if(name == "plastico"){
					ButtonAction.GetComponent<Button>().onClick.AddListener(delegate { playSound("sounds/Plastico"); });
					TextDescription.GetComponent<Text>().text = "Na Reciclagem Mecânica, que é a mais comum para os materiais plásticos pós-consumo, os resíduos passam por quatro etapas:\n\n" +
						"1. Fragmentação (moagem) – os resíduos são levados para um moinho que reduzem o seu tamanho.\n\n" +
						"2. Lavagem e Separação – os fragmentos (comumente chamados de flakes) são lavados com água e a separação é feita pela diferença de densidades, ou seja, os materiais mais densos afundam e os menos densos ficam na superfície da água.\n\n" +
						"3. Secagem – os flakes separados são secos em grandes secadores com circulação de ar quente.\n\n" +
						"4. Extrusão – os flakes secos são alimentados em uma máquina extrusora onde são fundidos por aquecimento e levados por uma rosca sem fim a uma matriz onde são formados os filamentos contínuos (comumente chamados de “espaguetes”) que são " +
						"resfriados em uma banheira com água a temperatura ambiente e são cortados em uma granuladora, formando os grânulos de material plástico reciclado que são embalados.";
					TextTargetName.GetComponent<Text> ().color = Color.red;
				
				}

				if(name == "vidro"){
					ButtonAction.GetComponent<Button>().onClick.AddListener(delegate { playSound("sounds/Vidro"); });
					TextDescription.GetComponent<Text>().text = "Etapas do processo de reciclagem de vidro\n\n" +
						"No lugar destinado à reciclagem, o vidro passa por cinco fases:\n\n" +
						"Limpar: esse processo visa retirar qualquer sujeira que ainda esteja impregnada no vidro e, para isso é realizada uma lavagem. Toda a água utilizada é posteriormente tratada e recuperada, com isso evita-se o desperdício e minimiza-se o risco de contaminar os recursos hídricos.\n   " +
						"Recolher impurezas: ao longo desta etapa o vidro percorre uma esteira para que sejam retiradas impurezas como pedras, metais e plásticos. O processo pode ser ainda mais eficaz se o material passar por um eletroímã, cuja função é a de separar metais que possam ser contaminantes.\n" +
						"Prensar e enfardar: nesta etapa o vidro passa por trituração, processo que consegue transformar cacos de vidro em pedaços homogêneos.\n  " +
						"Fundir: em temperatura acima de 1300 °C os cacos são então aquecidos e fundidos.\n   " +
						"Fazer o acabamento: agora o vidro já pode ser moldado e manuseado para compor novas embalagens, as quais terão como destino as indústrias e, consequentemente, o consumidor.";
					TextTargetName.GetComponent<Text> ().color = Color.green;
				
				}


			}
		}

		//function to play sound
		void playSound(string ss){
			clipTarget = (AudioClip)Resources.Load(ss);
			soundTarget.clip = clipTarget;
			soundTarget.loop = false;
			soundTarget.playOnAwake = false;
			soundTarget.Play();
		}
			
	}
}